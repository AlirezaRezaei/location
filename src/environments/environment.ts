// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl:'http://localhost:3000/',
  apiKey:'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjAxZTNjY2M0ODQ3YjI4NmYwNzY1YmJiYjkyNDJmNWE1ZjAyZGRmOWVjYzdiYWQ5OWYzY2JhYzhmZTI1MjNkNGU3MWE2MDBiODZjYTEyMjY0In0.eyJhdWQiOiIxMDU2NyIsImp0aSI6IjAxZTNjY2M0ODQ3YjI4NmYwNzY1YmJiYjkyNDJmNWE1ZjAyZGRmOWVjYzdiYWQ5OWYzY2JhYzhmZTI1MjNkNGU3MWE2MDBiODZjYTEyMjY0IiwiaWF0IjoxNTk4MTYwNjM2LCJuYmYiOjE1OTgxNjA2MzYsImV4cCI6MTYwMDg0MjYzNiwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.sS_-jkCIuR_Ok_J_o8G4x4jFJCocbgda_AQpPwUcV6uh400I4ONpdqaBFBp3Db7Y8IkMkzi2JHUiZ-dL49Ukle8_bPk6O-GvwSEIvhk9Qregz5EQZi0tKtgjm-FENkuQhecZY4MMuk13E2r0syFQdnO5AT4ISVIX7txgnbt7IHZxMmBns0TFCOP_FRWbyJ-ovp86VtOndZywEPyTafy1mtQBFt-sm4Enk_l5dak2BN5mSAZ7-H6YCU7GKoOk0kc1I5Jo7_Kpxg8mm28h0tulpm1ZW74zUeuNY07CJEEw1Nmnn8g8KuhaDS7gyoJAFdRale_RfGkMtofN7tgaaGc-ig',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
