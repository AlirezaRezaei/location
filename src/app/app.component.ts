import { Component } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { UserInfo } from './modules/login/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  showFiller = false;
  title = 'location-project';
  userInfo: UserInfo;
  public isLoggedIn: boolean;

  constructor(private authService: AuthService) {
    this.setUserInfo();
  }

  setUserInfo() {
    this.isLoggedIn = this.authService.isLoggedIn();
    this.userInfo = this.authService.userInfo();
  }

  loggedOut() {
    this.authService.loggedOut();
    this.isLoggedIn = false;
  }
}
