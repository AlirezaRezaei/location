export interface User{
    email:string,
    password:string
}

export interface UserInfo {
  email: string;
  iat: number;
  exp: number;
  sub: string;
}