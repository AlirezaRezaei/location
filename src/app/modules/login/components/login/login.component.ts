import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginApiCalls } from '../../services/login-api-calls.service';
import { AuthService } from './../../../../core/services/auth.service';
import { Router } from '@angular/router';
import { User } from './../../models/user.model';
import { SnackBarService } from 'src/app/core/services/snack-bar.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public userInfo: User;
  public isLoggedIn: Boolean;

  constructor(
    private loginApiCall: LoginApiCalls,
    private authService: AuthService,
    private _router: Router,
    private snackBar: SnackBarService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.checkLoggedIn();
  }

  initForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    this.userInfo = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value,
    };
    this.loginApiCall.login(this.userInfo).subscribe(
      (res) => {
        localStorage.setItem(
          'token',
          JSON.stringify(jwt_decode(res.accessToken))
        );
        location.reload();
      },
      (error) => {
        this.snackBar.showSnackBar('wrong email or password', 'warn', 4000);
      }
    );
  }

  checkLoggedIn() {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn) {
      this._router.navigate(['/']);
    }
  }
}
