import { TestBed } from '@angular/core/testing';

import { LoginApiCalls } from './login-api-calls.service';

describe('LoginAPiCallService', () => {
  let service: LoginApiCalls;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginApiCalls);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
