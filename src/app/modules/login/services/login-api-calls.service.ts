import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginApiCalls {
  public baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  login(userInfo): Observable<any> {
    let url = `${this.baseUrl}login`;
    return this.http.post(url, userInfo);
  }
}
