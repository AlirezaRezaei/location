import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileRouting}  from './profile-routing.module';
import { ProfileComponent } from './components/profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import {ProfileService} from './services/profile.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRouting,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers:[ProfileService]
})
export class ProfileModule { }
