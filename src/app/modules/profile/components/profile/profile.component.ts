import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackBarService } from 'src/app/core/services/snack-bar.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  public userForm: FormGroup;
  public userId: number;
  constructor(
    private profileService: ProfileService,
    private auth: AuthService,
    private snackBar: SnackBarService
  ) {}

  ngOnInit(): void {
    this.getUserDetail();
    this.initForm();
  }

  getUserDetail() {
    this.userId = this.auth.userInfo().sub;
    this.profileService.getUserDetail(this.userId).subscribe((res) => {
      this.setForm(res);
    });
  }

  initForm() {
    this.userForm = new FormGroup({
      id: new FormControl(this.userId, [Validators.required]),
      userId: new FormControl(this.userId, [Validators.required]),
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
    });
  }

  setForm(user) {
    this.userForm.patchValue({
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
    });
  }

  onSubmit() {
    this.profileService.editUser(this.userForm.value).subscribe((res) => {
      this.snackBar.showSnackBar('Successfully change', 'primary', 4000);
      this.setUserInfo(res);
      location.reload();
    });
  }

  setUserInfo(user) {
    let userInfo = JSON.parse(localStorage.getItem('token'));
    userInfo.email = user.email;
    localStorage.setItem('token', JSON.stringify(userInfo));
  }
}
