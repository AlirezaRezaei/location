import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicMessageComponent } from './components/public-message/public-message.component';
import { AddPublicAddressComponent } from './components/add-public-address/add-public-address.component';
import { PublicAddressDetailComponent } from './components/public-address-detail/public-address-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PublicMessageComponent,
  },
  {
    path: 'addPublicAddress',
    component: AddPublicAddressComponent,
  },
  {
    path: 'edit-address/:id',
    component: AddPublicAddressComponent,
  },
  {
    path: 'publicAddress/:id',
    component: PublicAddressDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicMessageRouting {}
