import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from 'src/app/core/models/address.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PublicMessageService {
  public baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getPublicAddress(): Observable<any> {
    let url = `${this.baseUrl}public-addresses`;
    return this.http.get(url);
  }

  createFavoriteAddress(favoriteAddress): Observable<any> {
    let url = `${this.baseUrl}favorite-addresses`;
    delete favoriteAddress.id;
    return this.http.post(url, favoriteAddress);
  }

  createPublicAddress(publicAddress: Address): Observable<any> {
    let url = `${this.baseUrl}public-addresses`;
    return this.http.post(url, publicAddress);
  }

  getPublicAddressDetail(id: number): Observable<any> {
    let url = `${this.baseUrl}public-addresses/${id}`;
    return this.http.get(url);
  }

  editPublicAddress(id: number, publicAddress: Address): Observable<any> {
    let url = `${this.baseUrl}public-addresses/${id}`;
    return this.http.put(url, publicAddress);
  }
}
