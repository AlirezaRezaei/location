import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicMessageService } from './services/public-message.service';
import { PublicMessageComponent } from './components/public-message/public-message.component';
import { PublicMessageRouting } from './public-message-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AddressCardModule } from './../../sharedModule/address-card/address-card.module';
import { MatButtonModule } from '@angular/material/button';
import { AddPublicAddressComponent } from './components/add-public-address/add-public-address.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MapComponent } from './components/map/map.component';
import { PublicAddressDetailComponent } from './components/public-address-detail/public-address-detail.component';

@NgModule({
  declarations: [
    PublicMessageComponent,
    AddPublicAddressComponent,
    MapComponent,
    PublicAddressDetailComponent,
  ],
  imports: [
    CommonModule,
    PublicMessageRouting,
    HttpClientModule,
    AddressCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
  ],
  providers: [PublicMessageService],
})
export class PublicMessageModule {}
