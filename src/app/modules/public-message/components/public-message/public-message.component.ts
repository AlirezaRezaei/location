import { Component, OnInit } from '@angular/core';
import { Address } from './../../../../core/models/address.model';
import { PublicMessageService } from '../../services/public-message.service';
import { SnackBarService } from 'src/app/core/services/snack-bar.service';

@Component({
  selector: 'app-public-message',
  templateUrl: './public-message.component.html',
  styleUrls: ['./public-message.component.scss'],
})
export class PublicMessageComponent implements OnInit {
  public publicList: Address[] = [];

  constructor(
    private publicMessageService: PublicMessageService,
    private snackBar: SnackBarService
  ) {}

  ngOnInit(): void {
    this.getPublicMessage();
  }

  getPublicMessage() {
    this.publicMessageService.getPublicAddress().subscribe(
      (res) => {
        this.publicList = res;
      },
      (error) => {
        this.snackBar.showSnackBar('cant fetch data', 'warn', 4000);
      }
    );
  }

  addToFavorite(address: Address) {
    let favoriteAddress = { ...address, userId: 1 };
    this.publicMessageService.createFavoriteAddress(favoriteAddress).subscribe(
      (res) => {
        this.snackBar.showSnackBar(
          'Successfully added to favorite',
          'primary',
          4000
        );
      },
      (error) => {
        this.snackBar.showSnackBar('Error ocurred !!', 'warn', 4000);
      }
    );
  }
}
