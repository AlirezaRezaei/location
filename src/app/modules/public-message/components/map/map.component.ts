import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Output, EventEmitter } from '@angular/core';

declare var Mapp: any;

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @Output() coordinate = new EventEmitter();
  @Input() latlng;
  public marker: any;
  public map: any;
  public currentRoute: string;
  public markerConfig: any = { type: '', draggable: false, latlng: {} };

  constructor(private _router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.currentRoute = this._router.url;
    this.initMap();
  }

  initMap() {
    this.map = new Mapp({
      element: '#map',
      presets: {
        latlng: {
          lat: 32,
          lng: 52,
        },
        zoom: 6,
      },
      locale: 'en',
      apiKey: environment.apiKey,
    });
    this.map.addLayers();
    this.setMarker();
    if(!this.latlng){
      this.latlng={lat: 32,
        lng: 52,}
    }
  }

  setMarker() {
    this.marker = this.map.addMarker({
      name: 'advanced-marker',
      latlng: this.latlng,
      icon: this.map.icons.red,
      pan: false,
      draggable: true,
      history: false,
      on: {
        click: function () {
          console.log('click');
        },
        contextmenu: function () {
          console.log('Contextmenu callback');
        },
      },
    });
    this.coordinate.emit(this.marker);
  }

  setCoordinate() {
    this.coordinate.emit(this.marker);
  }
}
