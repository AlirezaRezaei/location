import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPublicAddressComponent } from './add-public-address.component';

describe('AddPublicAddressComponent', () => {
  let component: AddPublicAddressComponent;
  let fixture: ComponentFixture<AddPublicAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPublicAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPublicAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
