import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicMessageService } from '../../services/public-message.service';
import { SnackBarService } from 'src/app/core/services/snack-bar.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-public-address',
  templateUrl: './add-public-address.component.html',
  styleUrls: ['./add-public-address.component.scss'],
})
export class AddPublicAddressComponent implements OnInit {
  public publicAddressForm: FormGroup;
  public isEdit: boolean;
  public addressId: number;
  public marker: any;
  public coordinate: any;
  public latlng;
  constructor(
    private publicMessageService: PublicMessageService,
    private snackBar: SnackBarService,
    private authService: AuthService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.checkRoute();
    this.getPublicAddress();
  }

  checkRoute() {
    if (this.route.snapshot.params.id) {
      this.addressId = this.route.snapshot.params.id;
      this.isEdit = true;
    } else {
      this.latlng = { lat: 37.375, lng: 49.759 };
    }
  }

  getPublicAddress() {
    if (this.isEdit) {
      this.publicMessageService
        .getPublicAddressDetail(this.addressId)
        .subscribe((response) => {
          this.setFormDetail(response);
        });
    }
  }

  setFormDetail(form) {
    this.publicAddressForm.patchValue({
      userId: this.authService.userInfo().sub,
      name: form.name,
      address: form.address,
      latitude: form.latitude,
      longiture: form.longiture,
    });
    this.latlng = {
      lat: form.latitude,
      lng: form.longiture,
    };
  }

  initForm() {
    this.publicAddressForm = new FormGroup({
      userId: new FormControl(''),
      name: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      latitude: new FormControl('', Validators.required),
      longiture: new FormControl('', Validators.required),
    });
  }

  onSubmit() {

    this.publicAddressForm.patchValue({
      latitude: this.coordinate._latlng.lat,
      longiture: this.coordinate._latlng.lng,
    });

    if (this.isEdit) {
      this.publicMessageService
        .editPublicAddress(this.addressId, this.publicAddressForm.value)
        .subscribe((res) =>
          this.snackBar.showSnackBar('Successfully change', 'primary', 4000)
        );
    } else {
      this.publicMessageService
        .createPublicAddress(this.publicAddressForm.value)
        .subscribe(
          (res) =>
            this.snackBar.showSnackBar('Successfully added', 'primary', 4000),
          (error) => {
            this.snackBar.showSnackBar('wrong Info !!', 'warn', 4000);
          }
        );
    }
  }

  getCoordinate($event) {
    this.coordinate = $event;
    this.publicAddressForm.patchValue({
      userId: this.authService.userInfo().sub,
      latitude: this.coordinate._latlng.lat,
      longiture: this.coordinate._latlng.lng,
    });
  }
}
