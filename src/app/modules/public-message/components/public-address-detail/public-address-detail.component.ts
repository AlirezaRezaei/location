import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicMessageService } from '../../services/public-message.service';
import { Address } from 'src/app/core/models/address.model';

@Component({
  selector: 'app-public-address-detail',
  templateUrl: './public-address-detail.component.html',
  styleUrls: ['./public-address-detail.component.scss'],
})
export class PublicAddressDetailComponent implements OnInit {
  public address: Address;
  public latlng;
  constructor(
    private router: ActivatedRoute,
    private addressService: PublicMessageService
  ) {}

  ngOnInit(): void {
    this.getAddressDetail();
  }
  getAddressDetail() {
    let id = this.router.snapshot.params.id;
    this.addressService.getPublicAddressDetail(id).subscribe((res) => {
      this.address = res;
      this.latlng = {
        lat: res.latitude,
        lng: res.longiture,
      };
    });
  }
}
