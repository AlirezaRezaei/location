import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicAddressDetailComponent } from './public-address-detail.component';

describe('PublicAddressDetailComponent', () => {
  let component: PublicAddressDetailComponent;
  let fixture: ComponentFixture<PublicAddressDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicAddressDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicAddressDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
