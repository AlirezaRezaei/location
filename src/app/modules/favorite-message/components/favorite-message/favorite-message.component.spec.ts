import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteMessageComponent } from './favorite-message.component';

describe('FavoriteMessageComponent', () => {
  let component: FavoriteMessageComponent;
  let fixture: ComponentFixture<FavoriteMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
