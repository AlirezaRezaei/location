import { Component, OnInit } from '@angular/core';
import { FavoriteAddress } from './../../models/favorite-address.model';
import { FavoriteAddressService } from './../../services/favorite-address.service';

@Component({
  selector: 'app-favorite-message',
  templateUrl: './favorite-message.component.html',
  styleUrls: ['./favorite-message.component.scss'],
})
export class FavoriteMessageComponent implements OnInit {
  public favoriteList: FavoriteAddress[] = [];

  constructor(private favoriteService: FavoriteAddressService) {}

  ngOnInit(): void {
    this.getFavoriteList();
  }

  getFavoriteList() {
    this.favoriteService.getFavoriteAddress().subscribe((res) => {
      this.favoriteList = res;
    });
  }
}
