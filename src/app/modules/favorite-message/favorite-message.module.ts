import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoriteMessageComponent } from './components/favorite-message/favorite-message.component';
import { FavoriteMessageRouting } from './favorite-message-routing.module';
import {FavoriteAddressService} from './services/favorite-address.service';
import { HttpClientModule } from '@angular/common/http';
import { AddressCardModule } from './../../sharedModule/address-card/address-card.module';

@NgModule({
  declarations: [FavoriteMessageComponent],
  imports: [
    CommonModule,
    FavoriteMessageRouting,
    HttpClientModule,
    AddressCardModule
  ],
  providers:[FavoriteAddressService]
})
export class FavoriteMessageModule { }
