import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FavoriteMessageComponent } from './components/favorite-message/favorite-message.component';

const routes: Routes = [
  {
    path: '',
    component: FavoriteMessageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoriteMessageRouting {}
