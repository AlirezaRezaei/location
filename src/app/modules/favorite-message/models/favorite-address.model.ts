import { Address } from './../../../core/models/address.model';

export interface FavoriteAddress extends Address {
  userId: number;
}
