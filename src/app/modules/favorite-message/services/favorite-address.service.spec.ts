import { TestBed } from '@angular/core/testing';

import { FavoriteAddressService } from './favorite-address.service';

describe('FavoriteAddressService', () => {
  let service: FavoriteAddressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FavoriteAddressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
