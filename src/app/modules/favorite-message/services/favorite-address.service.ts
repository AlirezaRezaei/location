import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FavoriteAddressService {
  public baseUrl: string = environment.baseUrl;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: localStorage.getItem('token'),
    }),
  };

  constructor(private http: HttpClient) {}

  getFavoriteAddress(): Observable<any> {
    let url = `${this.baseUrl}favorite-addresses`;
    return this.http.get(url,this.httpOptions);
  }
}
