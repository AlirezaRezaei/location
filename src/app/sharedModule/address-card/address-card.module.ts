import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressCardComponent } from './components/address-card/address-card.component';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [AddressCardComponent],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports:[AddressCardComponent]
})
export class AddressCardModule { }
