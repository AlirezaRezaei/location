import { Component, OnInit, Input } from '@angular/core';
import { Address } from 'src/app/core/models/address.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss'],
})
export class AddressCardComponent implements OnInit {
  @Input() addressDetail: Address;
  showMoreDetail: boolean;
  showEdit:boolean;
  constructor(private router: Router,private route:ActivatedRoute) {}

  ngOnInit(): void {
    this.checkRoute();
  }

  checkRoute() {
    if (this.router.url === '/' || this.router.url === '/favorite') {
      this.showMoreDetail = true;
    }else if(this.route.snapshot.params.id){
      this.showEdit = true;
    }
  }

  moreDetail(id: number) {
    this.router.navigate(['/publicAddress', id]);
  }

  edit(id: number) {
    this.router.navigate(['/edit-address', id]);
  }
}
